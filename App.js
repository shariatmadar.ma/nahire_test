import React, { Component } from 'react';

import EStyleSheet from 'react-native-extended-stylesheet';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
// import { ApolloClient, HttpLink, InMemoryCache } from 'apollo-client-preset';

import Auth from './src/screens/Auth';
import Pay from './src/screens/Pay';
import AuthLoading from './src/screens/AuthLoading';


EStyleSheet.build({
  $PURPLE: '#7F74C7',
  $LITEPURPLE: '#A6A8C3',
  $GREEN: '#03BECD',
  $DARKBLUE: '#3D4672',
  $GRAY: '#707070',
  $GRAY_BACK: '#F7F7F9',

  $FONT: 'IRANSansMobile',
  $BLACK_FONT: 'IRANSansMobile_Black',
  $ULTRALIGHT_FONT: 'IRANSansMobile_UltraLight',
  $LIGHT_FONT: 'IRANSansMobile_Light',
  $MEDIUM_FONT: 'IRANSansMobile_Medium',
  $BOLD_FONT: 'IRANSansMobile_Bold',
})

// const client = new ApolloClient({
//   link: new HttpLink({ uri: 'http://5.9.198.230:4000/graphql'}),
//   cache: new InMemoryCache()
// });

const client = new ApolloClient ({ uri: 'http://5.9.198.230:4000/graphql' });

const navigationOptions = {
  header: null,
  headerTransparent: true,
  headerTintColor: '#fff',
};

const AppStack = createStackNavigator({ Home:  { screen: Pay, navigationOptions }});
const AuthStack = createStackNavigator({ SignIn: { screen: Auth, navigationOptions }});

const AppContainer = createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoading,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  )
);

export default App = () => {
  return(
    <ApolloProvider client={client}>
      <AppContainer />
    </ApolloProvider>
  )
};