import React, { useEffect } from 'react';

import { View, ActivityIndicator, StatusBar, AsyncStorage } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const AuthLoading = ({navigation}) => {

    useEffect(() => {
        navigate();
    })

    const navigate = async () => {
        const userToken = await AsyncStorage.getItem('userToken');
        navigation.navigate(userToken ? 'App' : 'Auth');
    }

    return (
        <View style = { styles.container }>
            <StatusBar translucent backgroundColor="transparent" barStyle = { "dark-content" } />
            <ActivityIndicator />
        </View>
    );
};

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default AuthLoading;