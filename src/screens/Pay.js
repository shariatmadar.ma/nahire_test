import React, { Component } from 'react';

import { 
    View,
    Text,
    StatusBar,
    Image, 
    TouchableWithoutFeedback,
} from 'react-native';

import EStyleSheet from 'react-native-extended-stylesheet';
import Geolocation from 'react-native-geolocation-service';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';

import { Icon } from '../components/Icomoon';
import { IconedButton, Button } from '../components/Button';
import { CheckAndroidPermission, GetAndroidPermission } from '../Utils/Permissions';

class Pay extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lat: 0,
            lng: 0,
            locationEnabled: false,
        }
    }

    componentDidMount() {
        
    }

    getCurrentLocation = () => {
        CheckAndroidPermission().then((data) => {
            RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
            .then(data => {
                this.setState({
                    locationEnabled: true
                });
                Geolocation.getCurrentPosition(({ coords }) => {
                    console.log(coords.latitude)
                    console.log(coords.longitude)
                    this.setState({
                        lat: coords.latitude,
                        lng: coords.longitude,    
                    });
                });
            })
            .catch(err => {
                console.log(err)
            })
        }).catch(async (err) => {
            await GetAndroidPermission().then((data) => {
                RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
                .then(data => {
                    this.setState({
                        locationEnabled: true
                    });
                    Geolocation.getCurrentPosition(({ coords }) => {
                        console.log(coords.latitude)
                        console.log(coords.longitude)
                        this.setState({
                            lat: coords.latitude,
                            lng: coords.longitude,    
                        });
                    });
                })
                .catch(err => {
                    console.log(err)
                })
            }).catch((error) => {
                console.log("permission" + error.message);
            })
        });
        console.log(this.state.locationEnabled);
    };

    render() {
        return (
            <View style = { styles.container }>
                <StatusBar translucent backgroundColor="transparent" barStyle = { "dark-content" } />
                <View style = { styles.payContainer }>
                    <IconedButton 
                        text = { "پرداخت از طریق بارکد" }
                        style = {{
                            width: '47%',
                            marginTop: -12,
                            backgroundColor: EStyleSheet.value('$GREEN')
                        }}
                        textStyle = {{
                            fontSize: 10
                        }}
                        icon = { 'qrcode' }
                    />
                    <IconedButton 
                        text = { "پرداخت از طریق نام کاربری" }
                        style = {{
                            width: '47%',
                            marginTop: -12,
                            backgroundColor: EStyleSheet.value('$PURPLE')
                        }}
                        textStyle = {{
                            fontSize: 10
                        }}
                        icon = { 'user' }
                    />
                </View>
                <Image 
                    source = { require('../assets/images/location.png') }
                    style = { styles.locationImage }
                />
                {!this.state.locationEnabled? 
                    <View style = { styles.descContainer }>
                        <Text style = { styles.locationTitle }>سیستم موقعیت یاب شما غیر فعال است</Text>
                        <Text style = { styles.locationDesc }>جهت مشاهده کسب و کار های اطراف خود سیستم</Text>
                        <Text style = { styles.locationDesc }>موقعیت یاب را فعال کنید</Text>
                        <Button 
                            text = {"فعال سازی"}
                            style = { styles.btnActivate }
                            onPress = { () => this.getCurrentLocation() }    
                        />
                    </View>
                    :    
                    <View style = { styles.descContainer }>
                        <Text style = { styles.locationTitle }>سیستم موقعیت یاب شما فعال است</Text>
                    </View>
                }
                <Image 
                    source = { require('../assets/images/bottomnav.png') }
                    style = { styles.bottomNavImage }
                />
                <TouchableWithoutFeedback>
                    <View style = { styles.payBtnContainer }>
                        <View style = { styles.payBtn }>
                            <Icon name = {"card-payment"} size = {40} color = { EStyleSheet.value('$PURPLE') }/>
                            <Text style = { styles.payText }>پرداخت</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback>
                    <View style = { styles.tabSetting }>
                        <Icon name = {"setting"} size = {30} color = { EStyleSheet.value('$LITEPURPLE') }/>
                        <Text style = { styles.tabText }>تنظیمات</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback>
                    <View style = { styles.tabTransaction }>
                        <Icon name = {"fish"} size = {30} color = { EStyleSheet.value('$LITEPURPLE') }/>
                        <Text style = { styles.tabText }>تراکنش ها</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    };
};

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '$GRAY_BACK'
    },
    payContainer: {
        width: '100%',
        height: 100,
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
        elevation: 2,
        flexDirection: 'row',        
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        paddingBottom: 20,
        paddingHorizontal: 20
    },
    locationImage: {
        marginTop: '5%',
        height: '35%',
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    descContainer: {
        marginTop: '10%',
        paddingHorizontal: 30,
        alignItems: 'center'
    },
    locationTitle: {
        fontFamily: '$BOLD_FONT',
        color: '$LITEPURPLE',
        fontSize: 17
    },
    locationDesc: {
        marginTop: 10,
        fontFamily: '$FONT',
        color: '$LITEPURPLE',
        fontSize: 10,
        textAlign: 'center'
    },
    btnActivate: {
        width: '50%',
        marginTop: 20,
        alignSelf: 'center',
    },
    payBtnContainer: {
        alignItems: 'center',
        backgroundColor: '#FFF',
        width: 72,
        height: 72,
        borderRadius: 36,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        bottom: 25,
    },
    payBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 66,
        height: 66,
        borderRadius: 33,
        borderColor: '$PURPLE',
        borderWidth: 2,
    },
    payText: {
        fontFamily: '$FONT',
        fontSize: 8,
        color: '$PURPLE'
    },
    tabSetting: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 10,
        left: '5%',
        width: '25%',
    },
    tabTransaction: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 10,
        right: '5%',
        width: '25%',
    },
    tabText: {
        fontFamily: '$FONT',
        fontSize: 8,
        color: '$LITEPURPLE'
    },
    bottomNavImage: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        width: '100%',
        height: 60,
        resizeMode: 'cover',
    }
})

export default Pay;