import React, { useState } from 'react';

import {
    View,
    Text,
    StatusBar,
    Image,
    AsyncStorage
} from 'react-native';

import EStyleSheet from 'react-native-extended-stylesheet';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
// import { gql } from 'apollo-boost';
// import { useMutation } from '@apollo/react-hooks';

import { Button } from '../components/Button';
import { Avatar } from '../components/Avatar';
import { InputBox } from '../components/InputBox';

const Auth = (props) => {

    const [userName, setUserName] = useState("09364436650");
    const [password, setPassword] = useState("123123123");

    const handleSubmit = () => {
        console.log('entered');

        props.localLogin()
            .then(({ data }) => {
                console.log(data);
                AsyncStorage.setItem('userToken', data.localLogin._id);
                props.navigation.navigate('App');
            })
            .catch((error) => console.log(error));
        // const [{ data }] = useMutation(login);
        // console.log(data);
        // <Mutation mutation = { login }>
        //     {({error, loading, data}) => 
        //         {
        //             if (error) console.log(error);
        //             if (loading) console.log(loading);
        //             console.log(data)
        //             AsyncStorage.setItem('userToken', data.localLogin._id);
        //             navigation.navigate('App');
        //         }
        //     }
        // </Mutation>
    }

    return (
        <View style = { styles.container }>
            <StatusBar translucent backgroundColor="transparent" barStyle = { "dark-content" } />
            <Image 
                style = { styles.backgroundImage }
                source = {require('../assets/images/back.png')}
            />
            <Text style = { styles.title }>ورود به حساب کاربری</Text>
            <Avatar margin = { 16 }/>
            <View style = { [styles.userDataContainer, { marginTop: '15%' }] }>
                <Text style = { styles.userPassDesc }>نام کاربری</Text>
                <InputBox
                    style={styles.input} 
                    icon = { "tick" }
                    secondIcon = { "user" }
                    onChangeText = { (text) => setUserName(text) }
                />
                <Text style = { styles.info }>موجود است</Text>
            </View>
            <View style = { styles.userDataContainer }>
                <Text style = { styles.userPassDesc }>رمز عبور</Text>
                <InputBox
                    style={styles.input} 
                    icon = { "user-1" }
                    onChangeText = { (text) => setPassword(text) }
                />
            </View>
            <Button 
                text = {"ورود"}
                style = { styles.btnEnter }
                onPress = {() => handleSubmit()}    
            />
        </View>
    );
};

const styles = EStyleSheet.create({
    container: {
        flex: 1,
    },
    backgroundImage: {
        position: 'absolute',
        width:'100%',
        height: '100%',
        resizeMode: 'contain',
        top: 0,
        left: 0,
        // backgroundColor: '#000'
    },
    title: {
        fontFamily: '$MEDIUM_FONT',
        marginTop: 48,
        alignSelf: 'center',
        color: '$DARKBLUE',
        fontSize: 15,
    },
    userDataContainer: {
        marginVertical: '5%',
        marginHorizontal: 15,
    },
    info: {
        color: '$GREEN',
        fontFamily: '$FONT',
        fontSize: 12,
        marginTop: 5,
    },
    userPassDesc: {
        color: '$GRAY',
        fontFamily: '$FONT',
        fontSize: 13,
        marginBottom: 5,
    },
    input: {
        
    },
    btnEnter: {
        width: '90%',
        position: 'absolute',
        alignSelf: 'center',
        bottom: 32,
    }
})

export default graphql(
    gql`
        mutation login {
            localLogin(
                loginInput:
                {
                    emailOrPhoneNumber: "09364436650",
                    password: "123123123",
                    fcm:"RANDOM_STRING"
                }
            )
            {
                _id
            }
        }
    `, 
    {
        props: ({ mutate }) => ({
            localLogin: (loginInput) => mutate({ variables: { loginInput } })
        })
    }
)(Auth);