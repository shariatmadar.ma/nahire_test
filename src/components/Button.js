import React from 'react';
import { TouchableWithoutFeedback, View, Text } from 'react-native';

import EStyleSheet from 'react-native-extended-stylesheet';

import { Icon } from './Icomoon';

export const Button = ({
    text,
    onPress,
    style,
    textStyle,
    disable
}) => {
    return (
        <TouchableWithoutFeedback onPress = { disable ? null : onPress }>
            <View style = { [btnStyles.container, style] }>
                <Text style = { [btnStyles.text, textStyle] }>{ text }</Text>
            </View>
        </TouchableWithoutFeedback>
    );
};

export const IconedButton = ({
    text,
    onPress,
    margin,
    marginTop,
    padding,
    style,
    textStyle,
    icon,
    backgroundColor,
    disable
}) => {
    return (
        <TouchableWithoutFeedback onPress = { disable ? null : onPress }>
            <View style = { [icBtnStyles.container, style] }>
                <Text style = { [icBtnStyles.text, textStyle] }>{ text }</Text>
                <Icon name = { icon } size = { 20 } color = {'#FFF'} />
            </View>
        </TouchableWithoutFeedback>
    );
};

const btnStyles = EStyleSheet.create({
    container: {
        backgroundColor: '$PURPLE',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50, 
        borderRadius: 25,
    },
    text: {
        color: '#FFF',
        fontFamily: '$MEDIUM_FONT',
        fontSize: 15,
    }
})

const icBtnStyles = EStyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        height: 30,
        borderRadius: 15
    },
    text: {
        color: '#FFF',
        fontFamily: '$LIGHT_FONT',
        fontSize: 12,
        marginRight: 5,
    },
})