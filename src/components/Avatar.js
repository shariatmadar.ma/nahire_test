import React from 'react';
import { View, Image } from 'react-native';

import EStyleSheet from 'react-native-extended-stylesheet';
import { IconedButton } from './Button';

export const Avatar = ({ margin }) => {
    return (
        <View style = { [styles.container, { margin }] }>
            <View style = { styles.imageContainer }>
                <Image
                    source={require('../assets/images/user.png')}  
                    style={styles.image}
                />
            </View>
            <IconedButton 
                text = { "انتخاب عکس" }
                style = {{
                    paddingHorizontal: 10,
                    marginTop: -12,
                    backgroundColor: EStyleSheet.value('$GREEN')
                }}
                icon = { 'photo' }
            />
        </View>
    );
};

const styles = EStyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
    }, 
    imageContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        width: 100, 
        height: 100, 
        borderRadius: 50,
        borderColor: '$LITEPURPLE',
        borderWidth: 2,
        borderStyle: 'dashed',
        padding: 50,
    }, 
    image: {
        width: 100, 
        height: 100, 
        borderRadius: 50,
    }
})