import React, { useState } from 'react';

import {View, TextInput} from 'react-native';
import { Icon } from './Icomoon';
import EStyleSheet from 'react-native-extended-stylesheet';

export const InputBox = ({
    style,
    inputStyle,
    value,
    placeholder,
    keyboardType,
    maxLength,
    multiLine,
    numberOfLines,
    onChangeText,
    autoFocus,
    icon,
    secondIcon,
    editable,
    returnKeyType,
}) => {

    const [borderWidth, setBorderWidth] = useState(1);

    return (
        <View style = {[styles.container, {borderWidth}, style]}>
            <Icon name = {icon} size = {25} color = {EStyleSheet.value('$GREEN')}/>
            <View style = { styles.divider }></View>
            <TextInput
                style = {[styles.inputStyle, inputStyle]}
                value = {value}
                placeholder = {placeholder}
                placeholderTextColor = {EStyleSheet.value('#000')}
                underlineColorAndroid = "transparent"
                keyboardType = {keyboardType}
                maxLength = {maxLength}
                multiline = {multiLine || false}
                numberOfLines = {numberOfLines || 1}
                allowFontScaling = {false}
                onChangeText = {(text) => onChangeText(text)}
                selectionColor = {EStyleSheet.value('$GREEN')}
                autoFocus = {autoFocus}
                // onFocus = {setBorderWidth(1)}
                // onBlur = {setBorderWidth(0)}
                editable = {editable}
                returnKeyType = {returnKeyType || 'done'}
            />
            <Icon name = {secondIcon} size = {30} color = {EStyleSheet.value('$GREEN')}/>
        </View>
    );
};

const styles = EStyleSheet.create({
    container: {
        width: '100%',
        height: 50,
        borderRadius: 8,
        borderWidth: 1,
        // elevation: 1,
        borderColor: '$GREEN',
        flexDirection: 'row-reverse',
        paddingHorizontal: 5,
        alignItems: 'center',
    },
    divider: {
        width: 1,
        height : '60%',
        backgroundColor: '$GRAY',
        margin: 10,
    },
    inputStyle: {
        flex: 1,
        fontFamily: '$LIGHT_FONT',
        color: '#000',
        fontSize: 16,
        textAlign: 'right',
        marginRight: 5,
    },
});